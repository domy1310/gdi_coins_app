package net.gdi.android.tkalcec.myapplication.ui.coins

import android.R
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import net.gdi.android.tkalcec.myapplication.databinding.FragmentCoinsBinding


class CoinsFragment : Fragment() {

    private val coinsViewModel: CoinsViewModel by lazy {
        ViewModelProvider(this).get(CoinsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentCoinsBinding.inflate(inflater)

        binding.setLifecycleOwner(this)

        binding.coinsViewModel = coinsViewModel

        binding.rvCoins.adapter = CoinAdapter(CoinAdapter.OnCoinClickListener { coinMarket ->
            coinsViewModel.displayCoinDetails(coinMarket)
        })

        coinsViewModel.navigateToSelectedMarket.observe(viewLifecycleOwner, Observer {
            if (null != it) {
                this.findNavController().navigate(CoinsFragmentDirections.actionShowDetail(it, it.name))
                coinsViewModel.displayCoinDetailsComplete()
            }
        })

        return binding.root
    }

}
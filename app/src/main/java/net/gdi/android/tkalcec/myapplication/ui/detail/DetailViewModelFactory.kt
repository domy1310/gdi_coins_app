package net.gdi.android.tkalcec.myapplication.ui.detail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.gdi.android.tkalcec.myapplication.network.CoinMarket

class DetailViewModelFactory(
    private val coinMarket: CoinMarket,
    private val application: Application
) :ViewModelProvider.Factory{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            return DetailViewModel(coinMarket, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
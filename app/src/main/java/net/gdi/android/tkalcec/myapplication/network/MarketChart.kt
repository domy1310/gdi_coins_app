package net.gdi.android.tkalcec.myapplication.network

import com.squareup.moshi.Json

data class MarketChart(
    @Json(name = "prices")
    val prices: List<List<String>>,

    @Json(name = "market_caps")
    val marketCaps: List<List<String>>,

    @Json(name = "total_volumes")
    val totalVolume: List<List<String>>
) {

}
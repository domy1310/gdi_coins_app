package net.gdi.android.tkalcec.myapplication.ui.detail

import android.app.Application
import androidx.lifecycle.*
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.ValueFormatter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.gdi.android.tkalcec.myapplication.network.CoinApi
import net.gdi.android.tkalcec.myapplication.network.CoinMarket
import net.gdi.android.tkalcec.myapplication.network.MarketChart
import java.lang.Exception
import kotlin.collections.ArrayList

class DetailViewModel(coinMarket: CoinMarket, application: Application) : AndroidViewModel(application) {
    private val _selectedCoin = MutableLiveData<CoinMarket>()
    val selectedCoin: LiveData<CoinMarket>
        get() = _selectedCoin

    private val _marketChart = MutableLiveData<MarketChart>()
    val marketChart: LiveData<MarketChart>
        get() = _marketChart

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    val coinCurrentPriceString = Transformations.map(selectedCoin) {coin ->
        coin.currentPriceString
    }

    val coinPriceChangeString = Transformations.map(selectedCoin) {coin ->
        when {
            coin.priceChange24h > 0 -> coin.priceChange24hString + " (" + coin.priceChangePercentage24hString + ")"
            coin.priceChange24h < 0 -> coin.priceChange24hString + " (" + coin.priceChangePercentage24hString + ")"
            else -> coin.priceChange24hString + " (" + coin.priceChangePercentage24hString + ")"
        }
    }

    val coinMarketCapRank = Transformations.map(selectedCoin) {coin ->
        "#" + coin.marketCapRank
    }

    private val _chartTimeRange = MutableLiveData<Int>()
    val chartTimeRange: LiveData<Int>
        get() = _chartTimeRange


    init {
        _selectedCoin.value = coinMarket
        _chartTimeRange.value = DEFAULT_CHART_TIME_RANGE
        getMarketChart(DEFAULT_CHART_TIME_RANGE)
    }

    fun getMarketChart(days: Int) {
        coroutineScope.launch {
            var getPropertiesDeferred = _selectedCoin.value?.id?.let {
                CoinApi.retrofitService.getMarketCharts(it, days)
            }
            try {
                var marketChart = getPropertiesDeferred?.await()
                _chartTimeRange.value = days
                _marketChart.value = marketChart
            } catch (error: Exception) {
                _marketChart.value = null
            }
        }
    }

    class TimeValueFormatter(private val xAxisLabel: ArrayList<String>): ValueFormatter() {
        override fun getFormattedValue(value: Float): String {
            return value.toString()
        }

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            if(value.toInt() >= 0 && value.toInt() <= xAxisLabel.size - 1) {
                return xAxisLabel[value.toInt()]
            }
            else {
                return ("").toString()
            }
        }
    }

    companion object {
        const val DEFAULT_CHART_TIME_RANGE = 1
    }


}
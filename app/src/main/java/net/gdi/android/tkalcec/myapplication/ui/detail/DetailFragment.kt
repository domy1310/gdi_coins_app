package net.gdi.android.tkalcec.myapplication.ui.detail

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.github.mikephil.charting.charts.Chart
import net.gdi.android.tkalcec.myapplication.R
import net.gdi.android.tkalcec.myapplication.databinding.FragmentDetailBinding
import net.gdi.android.tkalcec.myapplication.ui.coins.CoinsViewModel


class DetailFragment : Fragment() {
    private lateinit var detailViewModel: DetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val application = requireNotNull(activity).application
        val binding = FragmentDetailBinding.inflate(inflater)
        binding.setLifecycleOwner(this)

        val coinMarket = DetailFragmentArgs.fromBundle(arguments!!).selectedCoin
        val viewModelFactory = DetailViewModelFactory(coinMarket, application)

        detailViewModel = ViewModelProvider(this, viewModelFactory).get(DetailViewModel::class.java)
        binding.detailViewModel = detailViewModel

        return binding.root
    }


}